

function entryLog(data, service='main'){
    console.log(`${service} - Http Status ####`,data.status);
    console.log(`${service} - Http Body ####`,data.body);
}

function create_UUID(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function expire_date(expires_in,idname) {
    let now = new Date();
    const moment = require("moment");
    let mydate = Number(moment(now).format("YYYYMMDDHHmmss"));

    if (mydate >= expires_in) {
      console.log(`Expired token_Bearer_${idname}`);
    } else {
      let token = postman.getEnvironmentVariable(`tokenBearer_${idname}`);
      pm.environment.set("tokenBearer", token);
      console.log(`valid token_Bearer_${idname}`);
    }
  }

  function when_expire_token(expires_in){
    const moment= require('moment');
    let now = new Date();
    let expiryDate =new Date(now.getTime() + expires_in *999.11);
    expiryDate = Number(moment(expiryDate).format("YYYYMMDDHHmmss"));
    return expiryDate;
}

function randomString(length, chars) {
  var mask = '';
  if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
  if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  if (chars.indexOf('#') > -1) mask += '0123456789';
  if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
  var result = '';
  for (var i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
  return result;
}

function metramd (myarray){
  let payment = Math.floor(Math.random() * myarray.length);
  return payment;
  
}

function formatDate (pminutes=0, _day,_month){
  let date = new Date();
  date = new Date(date.setTime(date.getTime() + (pminutes * 60 * 1000)));
  const hour = date.getHours().toString().padStart(2, "0");
  const seconds = date.getSeconds().toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");
  const day = _day || date.getDate();
  const month =  _month || (date.getMonth() + 1);
  const year = date.getFullYear();
  const formattedDate = `${year}-${month.toString().padStart(2, "0")}-${day.toString().padStart(2, "0")}T${hour}:${minutes}:${seconds}-03:00`;
  return formattedDate;
};

function millisToSeconds(time){
  return time; ///60000;
}

function writeFile(fileDire,newObj) {
  const fs = require('fs');
  
  let content = JSON.stringify(newObj); 
  console.log("content",content)
  return fs.writeFile(`${fileDire}`, content,'utf8', function (err) {
      if (err) throw err;
  });  
}


function createRandomDeviceName(prefix){
  let ts = Math.round((new Date()).getTime() / 1000);
  let serialNo = `${prefix}${ts}`
  return serialNo;
}


module.exports = {
    entryLog,
    create_UUID,
    randomString,
    metramd,
    formatDate,
    millisToSeconds,
    writeFile,
    createRandomDeviceName
}