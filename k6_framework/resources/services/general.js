
const baseUrlToken= __ENV.BASE_URL_AUTH0;
const baseUrlToken_b2c= __ENV.BASE_URL_AUTH0_B2C;
const baseUrlService = __ENV.BASE_URL;

module.exports= {
    baseUrlToken,
    baseUrlToken_b2c,
    baseUrlService
}