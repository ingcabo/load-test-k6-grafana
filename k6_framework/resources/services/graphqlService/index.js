
const url = 'gqlgateway';
const headers = {
    'content-type':'application/json'
}
let query= `
    query main {
        subMain {
        row
        }
    }
    `;
let variables= {};


const parametrization_test = {
    "smoke_test": {
        vus: 5,  // 5 user looping for 1 minute
        duration: '180s',
        thresholds: {
          'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: { 
            env: 'stage',    
            service: 'onboardingQuestionnaire',
            type_test: 'smoke_test'
        },
    },
    "load_test": {
        stages: [
            { duration: "2m", target: 30 },
            { duration: "5m", target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
            { duration: "10m", target: 60 }, // stay at 100 users for 10 minutes
            { duration: "5m", target: 0 }, // ramp-down to 0 users
          ],
          thresholds: {
            'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
            'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
          },
          tags: {    
              env: 'stage',      
              service: 'onboardingQuestionnaire',
              type_test: 'load_test' 
          },
    },
    "stress_test":{
        stages: [
            { duration: '2m', target: 100 }, // below normal load
            { duration: '5m', target: 100 },
            { duration: '2m', target: 200 }, // normal load
            { duration: '5m', target: 200 },
            { duration: '2m', target: 300 }, // around the breaking point
            { duration: '5m', target: 300 },
            { duration: '2m', target: 400 }, // beyond the breaking point
            { duration: '5m', target: 400 },
            { duration: '10m', target: 0 }, // scale down. Recovery stage.
          ],
        tags: {
            env: 'stage',
            service: 'graphqlService',
            type_test: 'stress_test' 
        },
    }
}

module.exports = {
    url,
    headers,
    query,
    variables,
    parametrization_test
}