
const url = 'wss://blablablablablablablablablabla.com/';

const parametrization_test = {
    "smoke_test": {
        vus: 54,  // 22 user looping for 5 minute
        duration: '10m',
        thresholds: {
          'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        },
        tags: { 
            env: 'stage',    
            service: 'onboardingQuestionnaire',
            type_test: 'smoke_test'
        },
        setupTimeout:'99900s'
    },
    "load_test": {
        stages: [
            { duration: "1m", target: 10 },
            { duration: "2m", target: 20 }, // simulate ramp-up of traffic from 1 to x users over x minutes.
            { duration: "3m", target: 30 }, // stay at x users for x minutes
            { duration: "4m", target: 40 }, // ramp-down to x users
            { duration: "5m", target: 55 }, 
          ],
          thresholds: {
            'Trend_Start_Session': ['p(99)<1500'], // 99% of requests must complete below 1.5s
            'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
          },
          tags: {    
              env: 'stage',      
              service: 'onboardingQuestionnaire',
              type_test: 'load_test' 
          },
          setupTimeout:'99900s'
    },
    "stress_test":{
        stages: [
            { duration: '2m', target: 100 }, // below normal load
            { duration: '5m', target: 100 },
            { duration: '2m', target: 200 }, // normal load
            { duration: '5m', target: 200 },
            { duration: '2m', target: 300 }, // around the breaking point
            { duration: '5m', target: 300 },
            { duration: '2m', target: 400 }, // beyond the breaking point
            { duration: '5m', target: 400 },
            { duration: '10m', target: 0 }, // scale down. Recovery stage.
          ],
        tags: {
            env: 'stage',
            service: 'webSocket',
            type_test: 'stress_test' 
        },
    },
    setupTimeout:'900s'
}

module.exports = {
    url,
    parametrization_test
}
