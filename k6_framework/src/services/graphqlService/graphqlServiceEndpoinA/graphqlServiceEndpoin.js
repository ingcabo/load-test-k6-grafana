import http from 'k6/http';
import {baseUrlService} from '../../../../resources/services/general.js'
import {query,variables,url,headers} from '../../../../resources/services/graphqlService/index.js'

export let execute = ({access_token}) => {
    
    headers['Authorization']= `Bearer ${access_token}`;
    var rq_data = JSON.stringify({ query: query,variables});

    let response = http.post(`${baseUrlService}${url}`, rq_data, { headers: headers })
    return response;
}