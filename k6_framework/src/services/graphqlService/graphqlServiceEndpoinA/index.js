import { check, group, sleep, fail } from 'k6';
import * as token_b2c from '../../token_b2c/token_b2c.js'
import * as graphqlServiceEndpoinData from '../../../../resources/services/graphqlService/index.js'
import * as graphqlServiceEndpoin from './graphqlServiceEndpoin.js'
import * as tools from '../../../../tools/index.js';

export let options = graphqlServiceEndpoinData.parametrization_test[__ENV.TYPE_TEST] 

export function setup() {

  let email='some@email.com';
  let {body} = token_b2c.execute(email);
  let {access_token} = JSON.parse(body);

  return {
    access_token,
  }
}

export default (data) => {
  let res = graphqlServiceEndpoin.execute(data)
  if (res.status != 200){
    tools.entryLog(res);
    //const {data} = res.json()
  }
  check(res, {
    'response code was 200': (res) => res.status == 200,
    'someField in array data': (res) => JSON.parse(res.body).data.onboardingQuestionnaire.length > 0,
  });
  sleep(1);

}

