import * as token_b2c from './token_b2c.js'
import * as token_b2c_data from '../../../resources/services/token_b2c.js'
import * as tools from '../../../tools/index.js';
import { check, sleep } from 'k6';

export let options = token_b2c_data.parametrization_test[__ENV.TYPE_TEST]

export default () => {
  let email= "some@gmail.com";
  let res = token_b2c.execute(email)
  if (res.status != 200){
    tools.entryLog(res);
  }

  check(res, { 'status was 200': r => r.status == 200 })
  sleep(1)
}
