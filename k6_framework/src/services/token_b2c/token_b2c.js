import http from 'k6/http';
import {base_url_token_b2c} from '../../../resources/services/general.js'
import {body,headers,url}  from '../../../resources/services/token_b2c.js'

export let execute = (email) => {
  
    body['username']=email;
    let response = http.post(`${base_url_token_b2c}${url}`, body, headers);

    return response;
}