import { randomString, randomIntBetween } from "https://jslib.k6.io/k6-utils/1.1.0/index.js";
import ws from 'k6/ws';
import { check, sleep } from 'k6';
import { Rate, Trend, Counter } from 'k6/metrics'
import * as token_b2c from '../token_b2c/token_b2c.js'
import * as tools from '../../../tools/index.js';
import { url } from '../../../resources/services/web_socket/index.js'
import * as web_socket_data from '../../../resources/services/web_socket/index.js'

let sessionDuration = 9900000
var interactionsSentCounter = new Counter('interactions_Counter');
var usersInSessionCounter = new Counter('sessions_Counter');
let usersConenected = [];


export let options = web_socket_data.parametrization_test[__ENV.TYPE_TEST]

const someVar =  __ENV.SOME_VAR
const userData = JSON.parse(open("../../../../data/yourFile.json"));
const interaction = ["interaction1","interaction2","interaction3"]


export function setup() { 

    userData.forEach(element => {
      let { emailId } = element;
      let { body } = token_b2c.execute(emailId);
      let { access_token } = JSON.parse(body);
      element['access_token'] = access_token;
      element['someVar'] = someVar;
    });
  
    return userData;
  }
  
  
export default function (userData) {
  let userIndex = __VU -1;  
  let {access_token,someVar,id} = userData[userIndex]; 

  //console.log('userData[userIndex]',JSON.stringify(userData[userIndex]));
  var excludedUserArr = userData.filter(item=>item.id !=id )

  let usertToIndex = tools.metramd(excludedUserArr);

  let urlw = `${url}?someVar=${someVar}`
  const commonMessage = JSON.stringify({
    event: "auth",
    jwt: access_token,
  });

 
  const interationMsg = JSON.stringify({
    event: "sendInteraction",
    toId: excludedUserArr[usertToIndex].id,
    interactionOption: interaction[tools.metramd(interaction)]
  })

  if (!usersConenected.includes(emailId))  {
  let res = ws.connect(urlw, null, function (socket) {
      socket.on('open', function open() {
      console.log(`VU ${userIndex} connected: `);
      usersConenected.push(emailId);
      socket.send(commonMessage);
      usersInSessionCounter.add(1);

      socket.setInterval(function timeout() {
        console.log(`VU ${userIndex} is sending an interaction: ${emailId} - ${id}  == ${interationMsg}`);
        socket.send(interationMsg);
        interactionsSentCounter.add(1);
      }, randomIntBetween(15000, 30000)); // say something every 0.25m-0.5m

    });

    socket.setInterval(function timeout() {
      socket.on('ping', function () {
        console.log(`VU ${userIndex} is sending PING!: `);
      });
    }, randomIntBetween(18000, 120000)); 

    socket.setInterval(function timeout() {
      socket.on('ping', function () {
        console.log(`VU ${userIndex} is sending PONG!: `);
      });
    }, randomIntBetween(18000, 120000)); 



    socket.on('message', function (message){
      let msg = JSON.parse(message);
       // console.log('====response===',JSON.stringify(msg))
    });


    socket.setTimeout(function () {
      console.log(`VU ${userIndex} is sClosing the socket: ${emailId} - ${status}`);
      socket.close();
    }, sessionDuration);
  });
  

  check(res, { 'Connected successfully': (r) => r && r.status === 101 });
  }
}