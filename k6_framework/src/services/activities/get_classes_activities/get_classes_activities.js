import http from 'k6/http';
import {service_base_url} from '../../../../resources/services/general.js'
import {body,url,headers} from '../../../../resources/services/activities_endpoint/activities_endpoint.js'

export let execute = ({access_token}) => {

    let url_param =`?limit=50&expandChildren=true`;
    let URL = `${service_base_url}${url}${url_param}`;
    headers['Authorization']= `Bearer ${access_token}`;
    let params ={
        headers
    }

    let response = http.get(URL,params)
    return response;
}