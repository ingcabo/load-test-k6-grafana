import { check, group, sleep, fail } from 'k6';
import * as token_b2c from '../../token_b2c/token_b2c.js'
import * as get_classes_activities_data from '../../../../resources/services/activities_endpoint/activities_endpoint.js'
import * as get_classes_activities from './get_classes_activities.js'
import * as tools from '../../../../tools/index.js';

export let options = get_classes_activities_data.parametrization_test[__ENV.TYPE_TEST] 

export function setup() {

  let email='332466@equinox.com';
  let {body} = token_b2c.execute(email);
  let {access_token} = JSON.parse(body);

  return {
    access_token,
  }
}

export default (data) => {
  let res = get_classes_activities.execute(data)
  if (res.status != 200){
    tools.entryLog(res);
    //const {data} = res.json()
  }
  check(res, {
    'response code was 200': (res) => res.status == 200,
    'activities array data': (res) => JSON.parse(res.body).data.length > 0,
  });
  sleep(1);

}